﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.IO;
using pruebaANDBL;
using System.Configuration;
using System.Text;

public partial class _Default : System.Web.UI.Page
{
    //Ruta de los archivos JSon de almacenamiento de información de parametrizacion y almacenamiento.
    private string rutaJson = ConfigurationManager.AppSettings["rutaJson"];    

    /// <summary>
    /// Cargar el listado de tramites mas usados, configurados en un archivo Json
    /// </summary>
    protected void cargarTramitesMasUsados()
    {
        string jsonTramites = rutaJson + "tramites.json";
        //Consultar las opiniones previamente almacenadas        
        if (File.Exists(jsonTramites))
        {
            StreamReader jsonStream = new StreamReader(jsonTramites, Encoding.Default);
            
            var jsonArchivo = jsonStream.ReadToEnd();            
            List<Tramite> lTramites = JsonConvert.DeserializeObject<List<Tramite>>(jsonArchivo);
            string carruselTramites = string.Empty;
            string carruselIndicador = string.Empty;
            
            int i = 0;
            foreach(Tramite registroTramite in lTramites)
            {
                if(i == 0)
                {
                    carruselTramites += "<div class='carousel-item active'>";
                    carruselIndicador += "<li data-target='#carouselIndicators' data-slide-to='" + i.ToString() +"' class='active'></li>";
                }
                else
                {
                    carruselTramites += "<div class='carousel-item'>";
                    carruselIndicador += "<li data-target='#carouselIndicators' data-slide-to='" + i.ToString() + "'></li>";
                }                    
                carruselTramites += "<div class='card govco-card'>";
                carruselTramites += "<div class='card-body mostUsed'>";
                carruselTramites += "Los trámites más usados";
                carruselTramites += "<span class='govco-icon govco-icon-plus' style='position: absolute; right: 5px;'></span>";
                carruselTramites += "</div>";
                carruselTramites += "<div class='card-body cardText'>";
                carruselTramites += registroTramite.TituloPrincipal;
                carruselTramites += "</div>";
                carruselTramites += "<div class='card-body otherText'>";
                carruselTramites += registroTramite.SubTitulo;
                carruselTramites += "</div>";
                carruselTramites += "<div class='card-boyd online'>";
                carruselTramites += "<span class='govco-icon govco-icon-tramite-en-linea' style='margin-left: 5 %; max-height: 1px; '></span>";
                carruselTramites += "Disponible en línea";                    
                carruselTramites += "</div>";
                carruselTramites += "</div>";
                carruselTramites += "<br />";
                carruselTramites += "<br />";                
                carruselTramites += "</div>";                    
                i += 1;
            }
            tramitesIndicador.InnerHtml = carruselIndicador;
            tramites.InnerHtml = carruselTramites;
            jsonStream.Close();
            jsonStream.Dispose();
        }             
    }

    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!IsPostBack)
        {
            //Cargar la información desde el JSON a la sección de Tramites mas usados:
            cargarTramitesMasUsados();            
        } 
    }


    /// <summary>
    /// Accion de almacenamiento en un archivo Json de las opiniones registradas
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEnviarOpinion_Click1(object sender, EventArgs e)
    {
        string jsonOpinion = rutaJson + "opiniones.json";
        //Consultar las opiniones previamente almacenadas
        List<Opinion> lOpiniones = new List<Opinion>();
        if (File.Exists(jsonOpinion))
        {
            StreamReader jsonStream = new StreamReader(jsonOpinion, Encoding.Default);

            var jsonArchivo = jsonStream.ReadToEnd();
            lOpiniones = JsonConvert.DeserializeObject<List<Opinion>>(jsonArchivo);
            jsonStream.Close();
            jsonStream.Dispose();
            File.Delete(jsonOpinion);
        }
        //Leer la nueva opinion ingresada
        Opinion nuevaOpinion = new Opinion();
        nuevaOpinion.OpinionRegistrada = txbOpiniones.Text;
        nuevaOpinion.FechaHora = DateTime.Today;

        lOpiniones.Add(nuevaOpinion);

        //Escribir las opiniones en el archivo Json
        string json = JsonConvert.SerializeObject(lOpiniones);
        System.IO.File.WriteAllText(jsonOpinion, json);

        lblMensajeOpinion.Attributes.Remove("hidden");        
        txbOpiniones.Text = string.Empty;
    }
}