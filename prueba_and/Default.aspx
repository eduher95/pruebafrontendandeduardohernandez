﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- css bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <!-- Custom CSS-->
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style.css">
    <!-- css CDN -->
    <link href="https://cdn.www.gov.co/v2/assets/cdn.min.css" rel="stylesheet">    

    <title>Prueba técnica AND - Eduardo Hernández Gómez</title>
</head>
<style>
    .test{
        background-color: black;
    }
  </style>
  <body>    
    <nav class="navbar navbar-expand-lg fixed-top navbar-govco navbar-expanded"><div class="navbar-container container"><div class="navbar-logo float-left"><a class="navbar-brand" href="https://www.gov.co/"><img src="https://cdn.www.gov.co/assets/images/logo.png" height="30" width="140" alt="Logo Gov.co"></a><button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapsible" aria-controls="navbarCollapsible" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button></div><div class="collapse navbar-collapse navbar-first-menu float-right"><div class="nav-primary mx-auto hidden-transition"><ul class="navbar-nav ml-auto nav-items nav-items-desktop"><li class="nav-item ">   </li><li class="nav-item ">   <a href="" class="nav-link">Trámites y servicios    </a></li><li class="nav-item ">   <a href="" class="nav-link">Tu Opinión Cuenta    </a></li><li class="nav-item ">   <a href="" class="nav-link">Entidades    </a></li><li class="nav-item ">   <a href="" class="nav-link">Noticias    </a></li><li class="nav-item "> </li></ul></div><div class="nav-item-primary ml-auto mr-2 is-scroll"><div class="form-inline form-search-bar my-2 my-lg-0 search-desktop-container"><input class="form-control mr-sm-2 search-navbar search-desktop-searchTerm searchTerm" type="search" placeholder="Buscar " aria-label="Search" name="searchTerm" id="search-desktop"><span class="govco-icon govco-icon-search-cn icon-search notranslate" style="display: block;"></span></div><span class="govco-icon govco-icon-language-en-n lang-govco ml-3"></span></div></div></div><div class="nav-secondary show-transition" id="nav-secondary"><div class="container"><div class="collapse navbar-collapse navbar-first-menu"><ul class="navbar-nav w-100 d-flex nav-items nav-items-desktop"><li class="nav-item ">  </li><li class="nav-item ">   <a href="/ficha-tramites-y-servicios/" class="nav-link">Trámites y servicios    </a></li><li class="nav-item ">   <a href="/tu-opinion-cuenta/" class="nav-link">Participación</a></li><li class="nav-item ">   <a href="/entidades/" class="nav-link">Entidades    </a></li><li class="nav-item ">   <a href="" class="nav-link">Noticias   </a></li><li class="nav-item ">   </li></ul></div></div></div><div class="navbar-nav navbar-notifications" id="notificationHeader"></div></nav>
    <header class="header">
        <div class="row pt-5">
            <div class="col-12 text-center mt-5 pt-4">
                <h1 class="headerText">
                    ¡Te damos la bienvenida!
                </h1>
            </div>
            <div class="col-12">
                <form>
                    <div class="col-12 text-center">    
                        <input type="text" class="roundInput" placeholder="Encuentra aquí lo que buscas" id="input-simple-govco" required>
                        <button type="button" class="btn-symbolic-govco align-column-govco buttonSearchBar">
                            <span class="govco-icon govco-icon-search-cn size-3x"></span>
                        </button>
                    </div>
                </form>
            </div>
            <div class="col-12 text-center pt-5">
                <h1 class="headerSecondText">
                    Toda la oferta del Estado colombiano a tu alcance
                </h1> 
            </div>
        </div>  
    </header>
      <form id="form1" runat="server">
          <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="containerx">
        <div class="row mb-5">
            <div class="col-4">
                <div id="carouselIndicators" class="carousel slide cardContainer mt-4 ml-6" data-ride="carousel">
                    <ol class="carousel-indicators" id="tramitesIndicador" runat="server">                      
                    </ol>
                    <div class="carousel-inner" runat="server" id="tramites">                          
                    </div>
                </div>
            </div>
            <div class="col-8 mb-3 mt-5">
                <div class="col-12 lineBottom">
                    <div class="col-12">
                        <h1 class="tramTitle">
                            Legalización de documentos de educación superior para adelantar estudios o trabajar en el exterior
                        </h1>
                    </div>
                    <div class="col-12">
                        <p class="tramPart">
                            Ministerio de Educación
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-3 online text-center">
                            <span class="govco-icon govco-icon-tramite-en-linea "></span>
                            Disponible en línea
                        </div>
                        <div class="col-6 tramCost text-center">
                            <span class="govco-icon govco-icon-tramite-con-costo size-2x"></span>
                            Trámite gratuito
                        </div>
                        <div class="col-3 tramRedir text-center">
                            <a href="https://www.google.com" class="tramRedir">Ver todo el trámite</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 lineBottom">
                    <div class="col-12">
                        <h1 class="tramTitle">
                            Adopción de un niño, niña o adolescente por persona, cónyuges o compañeros permanentes residentes en Colombia
                        </h1>
                    </div>
                    <div class="col-12">
                        <p class="tramPart">
                            Instituto Colombiano de Bienestar Familiar - ICBF
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-3 online text-center">
                            <span class="govco-icon govco-icon-tramite-en-linea "></span>
                            Disponible en línea
                        </div>
                        <div class="col-6 tramCost text-center">
                            <span class="govco-icon govco-icon-tramite-con-costo size-2x"></span>
                            Trámite gratuito
                        </div>
                        <div class="col-3 tramRedir text-center">
                            <a href="https://www.google.com" class="tramRedir">Ver todo el trámite</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 lineBottom">
                    <div class="col-12">
                        <h1 class="tramTitle">
                            Agendamiento de citas para consultorio jurídico
                        </h1>
                    </div>
                    <div class="col-12">
                        <p class="tramPart">
                            Ministerio de Justicia
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-3 online text-center">
                            <span class="govco-icon govco-icon-tramite-en-linea "></span>
                            Disponible en línea
                        </div>
                        <div class="col-6 tramCost text-center">
                            <span class="govco-icon govco-icon-tramite-con-costo size-2x"></span>
                            Trámite gratuito
                        </div>
                        <div class="col-3 tramRedir text-center">
                            <a href="https://www.google.com" class="tramRedir">Ver todo el trámite</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 lineBottom">
                    <div class="col-12">
                        <h1 class="tramTitle">
                            Certificados y constancias académicas
                        </h1>
                    </div>
                    <div class="col-12">
                        <p class="tramPart">
                            Servicio Nacional de Aprendizaje - SENA
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-3 online text-center">
                            <span class="govco-icon govco-icon-tramite-en-linea "></span>
                            Disponible en línea
                        </div>
                        <div class="col-6 tramCost text-center">
                            <span class="govco-icon govco-icon-tramite-con-costo size-2x"></span>
                            Trámite gratuito
                        </div>
                        <div class="col-3 tramRedir text-center">
                            <a href="https://www.google.com" class="tramRedir">Ver todo el trámite</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 moreTram text-right">
                <a>Conocer otros trámites <span class="govco-icon govco-icon-right-arrow size-1x"></span></a>
            </div>
        </div>
        <div class="row">
            <div class="col-8 ">
                <div class="col-12 ml-5 mr-5 lineBottom">
                    <div class="col-12 opinionTitle">
                        Queremos conocer tu opinión
                    </div>
                    <div class="col-3 ml-3 text-left textUnderColor" style="background:#F3561F 0% 0% no-repeat padding-box;">
                        Falta un día
                    </div>
                    <div class="col-12 opinionSubTitle">
                        Únete al Pacto por Colombia!
                    </div>
                    <div class="row ml-1">
                        <div class="col-5 opinionText text-left">
                            Presidencia de la República
                        </div>
                        <div class="col-5 opinionText text-right">
                            534 colombianos participando
                        </div>
                    </div>
                </div>
                <div class="col-12 ml-5 mt-3 mr-5 lineBottom">
                    <div class="col-3 ml-3 text-left textUnderColor" style="background:#069169 0% 0% no-repeat padding-box;">
                        Activo!
                    </div>
                    <div class="col-12 opinionSubTitle">
                        ¿Cómo mejorarías nuestro sistema de transporte?
                    </div>
                    <div class="row ml-1">
                        <div class="col-5 opinionText text-left">
                            Secretaría de Movilidad de Bogotá
                        </div>
                        <div class="col-5 opinionText text-right">
                            87 colombianos participando
                        </div>
                    </div>
                </div>
                <div class="col-12 ml-5 mt-3 mr-5 lineBottom">
                    <div class="col-4 ml-3 text-left textUnderColor" style="background:#3772FF 0% 0% no-repeat padding-box;">
                        Conoce los resultados
                    </div>
                    <div class="col-12 opinionSubTitle">
                        Los datos y visualizaciones del gobierno interesantes para su uso, aprovechamiento y toma de decisiones.
                    </div>
                    <div class="row ml-1">
                        <div class="col-12 opinionText text-left">
                            Ministerio de las Tecnologías de la Información y las Comunicaciones
                        </div>
                    </div>
                </div>
                <div class="col-12 moreTram text-right">
                    <a>Conocer otros ejercicios de participación <span class="govco-icon govco-icon-right-arrow size-1x"></span></a>
                </div>
            </div>
            <div class="col-4 mb-5">
                <div class="cardContainer mr-5">
                    <div class="card govco-card">
                        <div class="card-body secondCardTitle">
                            CUÉNTANOS
                        </div>
                        <div class="card-body secondCardSubTitle">
                            ¿Cómo podemos mejorar GOV.CO?
                        </div>
                        <div class="card-body secondCardText">
                            Estamos construyendo este espacio para ti y tu opinión nos ayuda a crecer.
                        </div>
                        <div class="card-body">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <div class="col-12 text-center"> 
                                        <asp:TextBox class="form-control" Rows="10" placeholder="Escribe aquí tu opinión" TextMode="MultiLine" ID="txbOpiniones" runat="server" MaxLength="255"></asp:TextBox>                                    
                                    </div>                                    
                                    <div class="col-12 text-center"> 
                                        <div class="alert-success-govco alert alert-dismissible fade show" aria-label="Alerta: caso de éxito" id="lblMensajeOpinion" runat="server" hidden="hidden">
                                          <button type="button" class="close" data-dismiss="alert" aria-label="Cerrar" title="Cerrar">&times;</button>
                                          <div class="alert-heading">
                                            <span class="govco-icon govco-icon-check-cn size-2x"></span>
                                            <span class="headline-l-govco">Tu opinión ha sido enviada</span>
                                          </div>
                                        </div>
                                    </div>    
                                    <div class="col-12 text-center">                                                                                
                                        <asp:Button ID="btnEnviarOpinion" runat="server" Text="Enviar mi opinión" class="btn btn-round btn-middle" OnClick="btnEnviarOpinion_Click1" />                                
                                    </div>    
                                </ContentTemplate>
                            </asp:UpdatePanel>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   </form>
    <footer>
        <div class="row pt-2">
            <div class="col-12 text-center">
                <h2 class="footerTitle">
                    Contáctanos
                </h2>
            </div>
        </div>
        <div class="row justify-content-center pt-5">
            <div class="col-1 text-center">
                <div class="col-12"><span class="govco-icon govco-icon-email iconsFooter"></span></div>
                <div class="col-12">
                    <p class="textIconsFooter">
                        Correo electrónico
                    </p>
                </div>
            </div>
            <div class="col-1 text-center">
                <div class="col-12"><span class="govco-icon govco-icon-callback-cp iconsFooter"></span></div>
                <div class="col-12">
                    <p class="textIconsFooter">
                        Regresamos <span class="textIconsFooter" style="white-space:nowrap;">tu llamada</span>  
                    </p>
                </div>
            </div>
            <div class="col-1 text-center">
                <div class="col-12"><span class="govco-icon govco-icon-call-center iconsFooter" ></span></div>
                <div class="col-12">
                    <p class="textIconsFooter">
                        Contacto telefónico
                    </p>
                </div>
            </div>
            <div class="col-1 text-center">
                <div class="col-12"><span class="govco-icon govco-icon-callin-cp iconsFooter"></span></div>
                <div class="col-12">
                    <p class="textIconsFooter">
                        Llamada web
                    </p>
                </div>
            </div>
            <div class="col-1 text-center">
                <div class="col-12"><span class="govco-icon govco-icon-wifi-cp iconsFooter"></span></div>
                <div class="col-12">
                    <p class="textIconsFooter">
                        Redes sociales
                    </p>
                </div>
            </div>
        </div>

        <div class="row justify-content-center pt-5 pb-5">
            <div class="col-2 text-center">
                <a href="https://google.com" class="aFooter">Términos de uso</a>
            </div>
            <div class="col-2 text-center">
                <a href="https://google.com" class="aFooter">Conoce a GOV.CO</a>
            </div>
            <div class="col-2 text-center">
                <a href="https://google.com" class="aFooter">Mapa del sitio</a>
            </div>
        </div>
    </footer>
    <!-- js jquery -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <!-- js bootstrap -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous">
    </script>
    <!-- utils.js CDN -->
    <script src="https://cdn.www.gov.co/v2/assets/js/utils.js"></script>
    <!--Custom Js-->
    <script src="js/custom.js"></script>
    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS-->
    <script type="text/javascript">
        utils.init();
        utils.countCharacter("textarea-example", 255);
    </script>           
  </body>
</html>
